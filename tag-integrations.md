Tradelist Tag Integrations Guide
========================

General Instructions and Notes
---------------
  
  In this guide we will provide tag integrations for different platforms, please apply the tags __given order__ and check carefully integration __code location__ and __variables__ to change.

  All variables shown with the __##VARIABLE_NAME##__, change them with the data from system and don't use __##__ before - after.

  For any question about the integrations do not hestiate to contact me from [ugur@awesomenyc.com](ugur@awesomenyc.com)
  
  
  ## Table of Contents
  
  <!-- MarkdownTOC autolink="true" bracket="round" markdown_preview="markdown" -->
  
  - [Google Tag Manager](#google-tag-manager)
  - [Google Search Console](#google-search-console)
  - [Google Analytics](#google-analytics)
  - [Google Analytics User-ID Integration](#google-analytics-user-iD-integration)
  - [Facebook Pixel](#facebook-pixel)
  - [Combined Event Snippets - SignUp](#combined-event-snippets-signup)
  - [Combined Event Snippets - LogIn](#combined-event-snippets-login)
  - [Combined Event Snippets - Create Listing](#combined-event-snippets-create-listing)
  - [Combined Event Snippets - View Listing](#combined-event-snippets-view-listing)
  - [Combined Event Snippets - Search](#combined-event-snippets-search)
  - [Combined Event Snippets - View Search Results](#combined-event-snippets-view-search-results)
  - [Credits](#credits)
  
  <!-- /MarkdownTOC -->
  
  ## Google Tag Manager <a name="google-tag-manager"></a>

  We will use Google Tag Manager as a main tool for making a lot of different integrations like, `Hotjar`, `Mouseflow`, `Quantcast Measure`, `Bing Ads Universal Tracking` and `Google Optimize`.

  Google Tag Manager integration code is a two parted code, first part must be integrate between __HEAD__ tags, second part is just after the __BODY__ opening tag.

  #### Between HEAD tags

  ```javascript
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-WDP5VK2');</script>
  <!-- End Google Tag Manager -->
  ```

  #### Just after BODY opening tag
  ```javascript
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WDP5VK2"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  ```

  ## Google Search Console

  Google Search Console ownership verifiying via DNS record. Please, sign in to your domain name provider (e.g. godaddy.com or namecheap.com), add the TXT record below into the DNS configuration for __tradelist.net__

  ```
  google-site-verification=qlTKtnO4EFXHe_P7khpz4aOoW7VhRiiyAy83m0YNuaM
  ```


  ## Google Analytics

  Right now on the site Google Analytics code is exists but for the funnels and goal definitions we need to integrate our own tag. With the Google Analytics tag, in these code snippet we are integrating Google Ads Conversions snippet too.

  Google Analytics tag must be integrate just after the __HEAD__ opening tag.

  #### Just after HEAD opening tag
  ```javascript
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-143236797-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-143236797-1');
    gtag('config', 'AW-729655576');
  </script>
  ```

  #### Google Analytics User-ID Integration

  If user logged in to the system, please provide __UserID__ _variable_ to Google Analytics for using advanced matching. This code can integrate with previous one as a adding last line _without_ __SCRIPT__ tags or can integrate anywhere in __BODY__ tags _with_ __SCRIPT__ tags. 

  ```javascript
  <script>
  gtag('set', {'user_id': '##USER_ID##'}); 
  </script>
  ```


  #### Facebook Pixel

  ```javascript
  <!-- Facebook Pixel Code -->
  <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '2362390287178459');
    fbq('track', 'PageView');
  </script>
  <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=2362390287178459&ev=PageView&noscript=1"
  /></noscript>
  <!-- End Facebook Pixel Code --></code></p>
  ```



  #### Combined Event Snippets - SignUp

  This code can integrate before closing __BODY__ tag or anywhere between of the __BODY__ tags. 
  
  ```javascript
  <!-- Event snippet for SignUp conversion page -->
  <script>
    gtag('event', 'conversion', {'send_to': 'AW-729655576/DNu_COK64qYBEJjS9tsC'});
    gtag('event', 'sign_up');
    fbq('track', 'CompleteRegistration', { value: 10, currency: 'USD' });  
  </script>
  ```

  #### Combined Event Snippets - Login

  This code can integrate before closing __BODY__ tag or anywhere between of the __BODY__ tags. 
  
  ```javascript
  <!-- Event snippet for Login conversion page -->
  <script>
    gtag('event', 'conversion', {'send_to': 'AW-729655576/BnsPCNSv3qYBEJjS9tsC'});
    gtag('event', 'login');
  </script>
  ```


  #### Combined Event Snippets - View Listing

  This code can integrate before closing __BODY__ tag or anywhere between of the __BODY__ tags. 
  
  ```javascript
  <!-- Event snippet for View Listing conversion page -->
  <script>
    gtag('event', 'conversion', {'send_to': 'AW-729655576/Am5fCI-N66YBEJjS9tsC'});
    var listitem = [           
      {
        "id": "##ITEM_ID##",
        "name": "##ITEM_NAME##",
        "list": "##ITEM_TRADER##",
        "category": "##ITEM_CATEGORY##",
        "list_position": "1",
        "price": "##ITEM_PRICE##",
        "currency": "##ITEM_CURRENCY##",
        "product_url": "##ITEM_URL##",
      }
    ];
    gtag('event', 'view_item', { items : listitem });
    fbq('track', 'ViewContent', {
      value: 1,
      currency: 'USD',
      content_ids: '"##ITEM_ID##',
      content_type: 'product',
    });  
  </script>
  ```

  #### Combined Event Snippets - Create Listing

  This code can integrate before closing __BODY__ tag or anywhere between of the __BODY__ tags. 
  
  ```javascript
  <!-- Event snippet for Create Listing conversion page -->
  <script>
    gtag('event', 'conversion', {'send_to': 'AW-729655576/PKo5CMmt3qYBEJjS9tsC'});
    gtag('event', 'generate_lead');  
    fbq('track', 'Lead', { value: 10, currency: 'USD'   });
    fbq('track', 'SubmitApplication');
  </script>
  ```

  #### Combined Event Snippets - Search

  This code can integrate before closing __BODY__ tag or anywhere between of the __BODY__ tags. 
  
  ```javascript
  <!-- Event snippet for Search conversion page -->
  <script>
    gtag('event', 'search', { search_term : '##SEARCH_STRING##' });
    fbq('track', 'Search', { search_string: '##SEARCH_STRING##' });
  </script>
  ```

  #### Combined Event Snippets - View Search Results

  For the search results _listitems_ variable is __must be array__! This code can integrate before closing __BODY__ tag or anywhere between of the __BODY__ tags. 
  
  ```javascript
  <!-- Event snippet for View Search Results conversion page -->
  <script>
    gtag('event', 'conversion', {'send_to': 'AW-729655576/sm50CLWu3qYBEJjS9tsC'});
    var listitems = [           
      {
        "id": "##ITEM_ID##",
        "name": "##ITEM_NAME##",
        "list": "##ITEM_TRADER##",
        "category": "##ITEM_CATEGORY##",
        "list_position": "1",
        "price": "##ITEM_PRICE##",
        "currency": "##ITEM_CURRENCY##",
        "product_url": "##ITEM_URL##",
      },
      ...,
      ...,
      ...
    ];
    gtag('event', 'view_item_list', { items : listitems });  
  </script>
  ```

  
  ## Credits
  
  This documentation created by [Ugur Yavuz](mailto:ugur@awesomenyc.com), for any question about the integration please send email.
  